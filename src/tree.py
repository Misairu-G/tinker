"""
Copyright 2020 Misairu G

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from typing import List, Tuple

from unittest import TestCase


class Node:
    def __init__(self, value: str, children: List = None):
        self.value = value

        if not children:
            self.children = list()
        else:
            self.children = children

    def __str__(self) -> str:
        return self.value

    def __repr__(self):
        return self.value


def traversal(root_node: Node) -> str:
    """Iterative depth-first traversal with parent info printing

    When meet a leaf node, print all its parents in the order of root-to-leaf

    :param root_node:
    :return:
    """
    stack: List[Node] = root_node.children
    parents = []

    results = []

    while stack:
        node = stack[-1]
        # if meet parent node again, means all children has been traversed
        if parents and parents[-1] == node:
            parents.pop()
            stack.pop()
            continue
        children = node.children

        if not children and not parents:  # leaf of root, ignore
            stack.pop()
            continue

        if not children and parents:  # leaf
            results.append("".join([str(p) for p in parents] + [str(node)]))
            stack.pop()
            continue

        if children:  # non-leaf
            parents.append(node)
            stack.extend(children)

    return "\n".join(sorted(results))


class TestTraversal(TestCase):
    def setUp(self):
        super(TestTraversal, self).setUp()

        self.data01 = Node('A', [
            Node('B', [
                Node('E'),
                Node('F')
            ]),
            Node('C', [
                Node('G', [
                    Node("H"),
                    Node('I')
                ]),
            ]),
            Node('D')
        ])

        self.data02 = Node('A', [
            Node('B', [
                Node('E'),
                Node('F')
            ]),
            Node('C', [
                Node('G', [
                    Node("H"),
                    Node('I')
                ]),
            ]),
            Node('D', [
                Node('J'),
                Node('K', [
                    Node('M'),
                    Node('N')
                ])
            ]),
            Node('L')
        ])

    def test_traversal_simple(self):
        self.assertEqual(
                traversal(self.data01),
                "\n".join(sorted(["BE", "BF", "CGH", "CGI"]))
        )

    def test_traversal_hard(self):
        self.assertEqual(
                traversal(self.data02),
                "\n".join(sorted(["BE", "BF", "CGH", "CGI", "DJ", "DKM", "DKN"]))
        )
